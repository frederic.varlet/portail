# utilisation des fonctions importées depuis code_cesar.py (cf. cesar.code_cesar() ligne 16)
import code_cesar as cesar


FICHIER_DESTINATION = "fichier_code.txt"

def code_fichier(fichier_texte):
    '''
    écrit dans FICHIER_DESTINATION la version coéde ROT13 du contenu de fichier_texte
    :param fichier_texte: (string) le nom du fichier à coder
    :return: None
    :effet de bord: crée et écrit le fichier fichier_code.txt
    '''
    destination = open(FICHIER_DESTINATION,"w",encoding="utf8")
    with open(fichier_texte,"r",encoding="utf8") as source:
        for ligne_texte in source:
            destination.write(cesar.code_phrase(ligne_texte,13)+'\n')


def gere_argument():    
    if len(sys.argv) == 2:
        return sys.argv[1]
    else:
        usage()
        sys.exit()

def usage():
    print('''
    usage : python3 code13_fichier nom_du_ficher_source
      
    exemples : 
      python3 code13_fichier.py anneau.txt
    ''') 


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    import sys
    nom_fichier_source = gere_argument()
    
    code_fichier(nom_fichier_source)
    print('fichier codé écrit dans '+ FICHIER_DESTINATION)
 
 